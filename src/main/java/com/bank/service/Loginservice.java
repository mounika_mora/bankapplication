package com.bank.service;

import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.bank.dto.LogInStatus;
import com.bank.dto.LoginDto;
import com.bank.dto.ResponseDto;
import com.bank.entity.CustomerRegistration;
import com.bank.exception.ProfileNotFoundException;
import com.bank.repository.CustomerRegistrationRepository;
import com.bank.repository.LoginRepository;

import lombok.AllArgsConstructor;

@AllArgsConstructor
@Service

public class Loginservice {

	private LoginRepository loginrepository;
	
	private CustomerRegistrationRepository customerRegistrationRepository;

	public ResponseDto login(LoginDto loginDto) {
		org.slf4j.Logger logger = LoggerFactory.getLogger(getClass());
		System.out.println(loginDto.toString());
		CustomerRegistration customerRegistration = customerRegistrationRepository.findByCustomerIdAndPassword(loginDto.getCustomerId(), 
				loginDto.getPassword());
		System.err.println(customerRegistration);
		if (customerRegistration != null && loginDto.getPassword().equals(customerRegistration.getPassword())) {
			customerRegistration.setLoggedIn(LogInStatus.LOGIN);
			customerRegistrationRepository.save(customerRegistration);
			logger.info("Profile Logged in successfully");
			return new ResponseDto("Logged In successfully",201);
		} else {
			logger.error("Invalid Credentials or profile Not found");
			throw new ProfileNotFoundException("Invalid Credentials or profile Not Found.");
		}
	}

}
