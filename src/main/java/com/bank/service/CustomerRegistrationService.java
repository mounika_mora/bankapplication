package com.bank.service;

import com.bank.dto.CustomerResgistrationDto;
import com.bank.dto.ResponseDto;

public interface CustomerRegistrationService {
	ResponseDto register(CustomerResgistrationDto customerResgistrationDto);
}
