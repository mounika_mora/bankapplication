package com.bank.service;

import java.util.Optional;

import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bank.dto.CustomerResgistrationDto;
import com.bank.dto.LogInStatus;
import com.bank.dto.ResponseDto;
import com.bank.entity.CustomerRegistration;
import com.bank.exception.UserAlreadyExists;
import com.bank.repository.CustomerRegistrationRepository;

@Service
public class CustomerRegistrationServiceImpl implements CustomerRegistrationService {
	@Autowired
	CustomerRegistrationRepository customerRegistrationRepository;

	public CustomerRegistrationServiceImpl(CustomerRegistrationRepository customerRegistrationRepository) {
		super();
		this.customerRegistrationRepository = customerRegistrationRepository;
	}

	@Override
	public ResponseDto register(CustomerResgistrationDto customerResgistrationDto) {
		Optional<CustomerRegistration> user = customerRegistrationRepository
				.findByAdhaarNo(customerResgistrationDto.getAdhaarNo());
		if (user.isPresent()) {
			throw new UserAlreadyExists("User already Registered");
		}
		CustomerRegistration customer = new CustomerRegistration();
		customer.setFirstName(customerResgistrationDto.getFirstName());
		customer.setLastName(customerResgistrationDto.getLastName());
		customer.setAdhaarNo(customerResgistrationDto.getAdhaarNo());
		customer.setAddress(customerResgistrationDto.getAddress());
		customer.setContactNo(customerResgistrationDto.getContactNo());
		customer.setLoggedIn(LogInStatus.LOGGEDOUT);
		String characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789~`!@#$%^&*()";
		String pwd = RandomStringUtils.random(8, characters);
		customer.setPassword(pwd);
		String characters2 = "0123456789";
		String id = RandomStringUtils.random(6, characters2);
		String id2 = "CID" + id;
		customer.setCustomerId(id2);
		String characters3 = "0123456789";
		String ano = RandomStringUtils.random(16, characters3);
		customer.setAccountNo(ano);
		customer.setBalance(5000);
		customer.setBankName("HDFC Bank");
		customer.setBranch("Karur");
		customer.setIfscCode("HDFC001");
		customerRegistrationRepository.save(customer);
		return ResponseDto.builder().httpStatus(201).message("User Registered sucessfully").build();
	}

}
