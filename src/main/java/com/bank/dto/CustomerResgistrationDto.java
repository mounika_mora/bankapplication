package com.bank.dto;


import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Pattern;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CustomerResgistrationDto {
	@NotBlank(message = "firstname is requiredfield")
	private String firstName;
 
	private String lastName;
	
	@NotBlank(message = "mobileNumber is required")
	@Size(min = 10, max = 10)
	private String contactNo;
 
	@NotBlank(message = "aadhar number is required")
	@Pattern(regexp = "[\\d]{12}", message = "invalid aadhar")
	private String adhaarNo;
 
	private String address;
}
