package com.bank.dto;

import jakarta.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Builder
public class BeneficiaryDTO {
	 
	@NotBlank(message = "Beneficiary name cannot be blank")
    private String beneficiaryName;
    
    @NotBlank(message = "Beneficiary account number cannot be blank")
    private String beneficiaryAccountNo;
    
    @NotBlank(message = "Beneficiary bank name cannot be blank")
    private String beneficiaryBankName;
    
    @NotBlank(message = "Beneficiary branch cannot be blank")
    private String beneficiaryBranch;
    
    @NotBlank(message = "Beneficiary IFSC code cannot be blank")
    private String beneficiaryIFSCCode;
}