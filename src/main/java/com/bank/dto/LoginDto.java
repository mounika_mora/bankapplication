package com.bank.dto;

import jakarta.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class LoginDto {
 
	@NotBlank(message = "Customer ID cannot be empty")
	private String customerId;
	@NotBlank(message = "Password cannot be empty")
	private String password;
}
