package com.bank.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bank.dto.BeneficiaryDTO;
import com.bank.dto.BeneficiaryResponseDTO;
import com.bank.service.BeneficiaryService;

import jakarta.validation.Valid;
import lombok.AllArgsConstructor;

@AllArgsConstructor
@RestController
@RequestMapping("/beneficiary")
public class BeneficiaryController {

	private BeneficiaryService beneficiaryService;

	@PostMapping("/{customerId}")
	public ResponseEntity<BeneficiaryResponseDTO> addBeneficiary(@PathVariable String customerId,
			@RequestBody @Valid BeneficiaryDTO beneficiaryDTO) {

		BeneficiaryResponseDTO responsedto = beneficiaryService.addBeneficiary(customerId, beneficiaryDTO);
		return new ResponseEntity<>(responsedto, HttpStatus.CREATED);
	}
}