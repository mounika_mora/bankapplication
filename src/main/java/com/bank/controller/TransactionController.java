package com.bank.controller;

import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bank.dto.loginresponse;
import com.bank.entity.Beneficiary;
import com.bank.entity.CustomerRegistration;
import com.bank.exception.InsufficientBalanceException;
import com.bank.repository.BeneficiaryRepository;
import com.bank.repository.CustomerRegistrationRepository;

@RestController
@RequestMapping("/fundtransfer")
@Validated
public class TransactionController {
	private final CustomerRegistrationRepository customerRepository;
	private final BeneficiaryRepository beneficiaryRepository;

	public TransactionController(CustomerRegistrationRepository customerRepository,
			BeneficiaryRepository beneficiaryRepository) {
		this.customerRepository = customerRepository;
		this.beneficiaryRepository = beneficiaryRepository;
	}

	@PostMapping("/{customerId}/{beneficiaryId}/{amount}")
	public ResponseEntity<loginresponse> transferAmount(@PathVariable String customerId,
			@PathVariable Long beneficiaryId, @PathVariable double amount) {
		if (amount <= 0) {
			throw new InsufficientBalanceException("Amount must be greater than zero.");
		}
		CustomerRegistration customer = customerRepository.findByCustomerId(customerId)
				.orElseThrow(() -> new ResourceNotFoundException("Customer not found with ID: " + customerId));
		Beneficiary beneficiary = beneficiaryRepository.findByBeneficiaryId(beneficiaryId);
		if (beneficiary == null) {
			throw new ResourceNotFoundException("Beneficiary not found with ID: " + beneficiaryId);
		}
		if (customer.getBalance() < amount) {
			throw new InsufficientBalanceException("Insufficient balance for customer: " + customerId);
		}
		double updatedBalance = customer.getBalance() - amount;
		customer.setBalance(updatedBalance);
		customerRepository.save(customer);

		return ResponseEntity.ok(new loginresponse("Amount transferred successfully."));
	}

}
